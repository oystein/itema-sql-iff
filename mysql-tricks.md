MySQL Tricks
============

Set pager
----------
\P less

Select showing records (vertical view)
--------------------------------------
SELECT * from movie \G;

To enable by default:
$ mysql --vertical
..or
$ mysql --auto-vertical-output
