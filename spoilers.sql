-- Actors per movie
select movie_id, count(distinct person_id) as actors from movie m join movie_cast mc using (movie_id) group by movie_id

-- snittskuespillere

create temporary table actorscount 
select movie_id, count(distinct person_id) as actors 
from movie m 
join movie_cast mc using (movie_id) 
group by movie_id;

create temporary table genrestats 
select mg.genre_id, g.genre_name, avg(ac.actors) as mean 
from movie_genres mg 
join actorscount ac using (movie_id) 
join genre g using (genre_id)
group by mg.genre_id 
order by mean desc;

select m.title, ac.actors, gs.mean, ac.actors - gs.mean as diff 
from movie m 
join actorscount ac using (movie_id) 
join movie_genres mg using (movie_id) 
join genrestats gs using (genre_id) 
order by diff desc 
limit 10;
