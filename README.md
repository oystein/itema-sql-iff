Itema SQL
=========

```bash
mkdir mysql-data
docker run --name mysql -v `readlink -f mysql-data`:/var/lib/mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=foobar -d mysql

HUSK: IKKE bruk sa eller root i verken dev eller prod!!!

mysql -h 127.0.0.1 -u root -p < sample_data_movies_mysql.sql

mysql -h 127.0.0.1 -u root -p movies
```

Litt SQL

```SQL
show databases;
use movies;

show tables;

describe movie;
describe movie_cast;
describe person;
```

Hent data fra movie-tabellen

```SQL
SELECT *
FROM movie;
```
For finere visning
```SQL
SELECT * FROM movie \G
```

Hent data
---------
```SQL
SELECT *
FROM movie
LIMIT 10;
```

Koble på cast
-------------
```SQL
SELECT m.movie_id, m.title, mc.person_id
FROM movie m
JOIN movie_cast mc ON (m.movie_id = mc.movie_id)
LIMIT 10;
```

Koble på person
---------------
```SQL
SELECT m.movie_id, m.title, mc.person_id
FROM movie m
JOIN movie_cast mc ON (m.movie_id = mc.movie_id)
JOIN person p USING (person_id)
LIMIT 10;
```

Skuespillere per film
---------------------
```SQL
SELECT m.movie_id, m.title, COUNT(mc.person_id) as actors
FROM movie m
JOIN movie_cast mc ON (m.movie_id = mc.movie_id)
JOIN person p USING (person_id)
GROUP BY m.movie_id
LIMIT 10;
```

```SQL
SELECT
  m.movie_id,
  m.title,
  COUNT(mc.person_id) as actors
FROM movie m
JOIN movie_cast mc ON (m.movie_id = mc.movie_id)
JOIN person p USING (person_id)
GROUP BY m.movie_id
ORDER BY actors DESC
LIMIT 10;
```
